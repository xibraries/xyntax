#!/bin/bash

set -xe

mkdir -p /usr/local/include/xolatile

cp xyntax.h /usr/local/include/xolatile/xyntax.h

exit
